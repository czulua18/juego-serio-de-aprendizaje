﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    [SerializeField]
    private float speed;

    private Coroutine click;
    private int indexPlanets = 0;

    [SerializeField]
    private Camera cam;

    public void ClickDetector()
    {
        if (click == null)
        {
            click = StartCoroutine(Click());
        }
        else
        {
            LastPlanet();
        }
    }

    private IEnumerator Click()
    {
        yield return new WaitForSeconds(0.2f);
        NextPlanet();
        click = null;
    }

    private void NextPlanet()
    {
        if (indexPlanets < 8)
        {
            indexPlanets++;

            transform.position = new Vector3(0f, 0, GameControl.instance.Planets[indexPlanets].transform.position.z - 
                15);
            GameControl.instance.AudioControl.PlayRecording(indexPlanets + 1);
        }
        
    }

    private void LastPlanet()
    {
        if (indexPlanets > 0)
        {
            indexPlanets--;
            if (indexPlanets > 0)
            {
                transform.position = new Vector3(0f, 0, GameControl.instance.Planets[indexPlanets].transform.position.z -
                15);
            }
            else
            {
                transform.position = GameControl.instance.InitialPos;
            }
            GameControl.instance.AudioControl.PlayRecording(indexPlanets + 1);
        }
        StopCoroutine(click);
        click = null;
    }
}
