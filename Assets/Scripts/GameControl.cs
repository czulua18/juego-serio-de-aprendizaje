﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour {

    [SerializeField]
    private GameObject cam;

    private CameraControl Ccont;

    public static GameControl instance;

    [SerializeField]
    private GameObject[] planets;

    [SerializeField]
    private Transform initialPos;

    private AuidoControl audioControl; 
	// Use this for initialization
	void Awake () {
        if (instance)
        {
            Destroy(instance);
        }
        instance = this;
        audioControl = GetComponent<AuidoControl>();
        Ccont = Instantiate(cam, initialPos.position, Quaternion.identity).GetComponent<CameraControl>();
	}

    private void Start()
    {
        StartCoroutine(UntilTheRecordFinished());     
    }

    IEnumerator UntilTheRecordFinished()
    {
        audioControl.PlayRecording(0);
        yield return new WaitForSeconds(28f);
        audioControl.PlayRecording(1);
        GetComponent<InputControl>().enabled = true;
    }

    public CameraControl CCont
    {
        get
        {
            return Ccont;
        }
    }

    public GameObject[] Planets
    {
        get
        {
            return planets;
        }
    }

    public Vector3 InitialPos
    {
        get
        {
            return initialPos.position;
        }
    }

    public AuidoControl AudioControl
    {
        get
        {
            return audioControl;
        }
    }
}
