﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuidoControl : MonoBehaviour {

    [SerializeField]
    private AudioClip[] soundS;
    [SerializeField]
    private AudioSource aS;
    
    public void PlayRecording(int i)
    {
        aS.clip = soundS[i];
        aS.Play();
    }
}
