﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputControl : MonoBehaviour {

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                GameControl.instance.CCont.ClickDetector();
            }
            
        }
    }

}
